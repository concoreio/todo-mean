import {SHOW_ALL} from '../constants/TodoFilters';
import {initialTodo} from '../todos/todos';

class HomeController {
  constructor() {
    this.todos = [initialTodo];
    this.filter = SHOW_ALL;
  }
}

export const Home = {
  template: require('./Home.html'),
  controller: HomeController
};
