import {SHOW_ALL} from '../constants/TodoFilters';
import {initialTodo} from '../todos/todos';

class TasksController {
  constructor() {
    this.todos = [initialTodo];
    this.filter = SHOW_ALL;
  }
}

export const Tasks = {
  template: require('./Tasks.html'),
  controller: TasksController
};
